My name is Stephen McNally.  I am currently enrolled as a non-degree seeking graduate student who will transfer into the new Data Science 
and Engineering PhD program once it is available in Fall of 2017.  I work as the Operations Manager for the National Center for Computational
Sciences at ORNL.  In this role I manage the day-to-day activities of our supercomputing center, which is centered around Titan.  My 
undergraduate degree is in Computer Information Systems and my Masters degree is in Business Administration.  I have a business oriented 
educational background with 10 or so years experience as a Linux system administrator working on some of the most powerful HPC systems in 
the world.  I am most interested in exploring operational data to better understand how seemingly disparate data have relationships.